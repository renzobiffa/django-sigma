from django.db import models

# Create your models here.


class Store(models.Model):

	domain = models.CharField(max_length=100, null=False, blank=False)
	name = models.CharField(max_length=100, null=False, blank=False)

	class Meta:
		verbose_name_plural = "Stores"

	def __unicode__(self):
		return self.name