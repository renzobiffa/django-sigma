from django.contrib import admin
from django.urls import include, path
from django.conf.urls.static import static
from django.conf import settings



# IMPORT CUSTOM APPS
from website.views import *


urlpatterns = [

	#SEO plese modify the folowing google verification code according to your setup
	#url(r'^google4a56b499e9f90188\.html$', lambda r: HttpResponse("google-site-verification: google4a56b499e9f90188.html", content_type="text/plain")),
	path('robots/', lambda r: HttpResponse("User-agent: *\nDisallow: ", content_type="text/plain")),


	# admin
	path('admin/', admin.site.urls),

	#website views
	path('index/', index),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)