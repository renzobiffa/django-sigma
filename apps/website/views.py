# -*- coding: utf-8 -*-
# froms
from django.shortcuts import render
from django.template.context_processors import csrf
from django.http import HttpResponseRedirect, HttpResponse


# HTTP Error 404
def page_not_found(request):

    context= {}
    return render(request, '404.html', context)


def index(request):
    context = set_template_variables(request)
    context.update(csrf(request))
    return render(request, 'index.html', context)


def set_template_variables(request):

    context = {}
    return context